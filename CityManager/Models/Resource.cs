﻿using CityManager.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CityManager.Models
{
    public class Resource : ModelBase
    {
        public int Quantity { get; set; }
        public int MaxQuantity { get; set; }
        public int Production { get; set; }
        public String Type { get; set; }

        public Resource(int quantity, int maxQuantity, int production, string type)
        {
            Quantity = quantity;
            MaxQuantity = maxQuantity;
            Production = production;
            Type = type;
        }
        public Resource()
        {
        }
    }
}
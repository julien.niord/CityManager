﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CityManager.Models
{
    public class Happiness : Resource
    {
        [NotMapped]
        public int MinThreshold { get; set; }
        public Happiness(int quantity, int maxQuantity, int production, string type, int minThreshold) : base(quantity, maxQuantity, production, type)
        {
            Quantity = quantity;
            MaxQuantity = maxQuantity;
            Production = production;
            Type = type;
            MinThreshold = minThreshold;
        }
        public Happiness()
        {
        }
    }
}
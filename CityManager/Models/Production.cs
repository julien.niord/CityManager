﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CityManager.Models
{
    public class Production : Building
    {


        public String resourceIn { get; set; }
        public String resourceOut { get; set; }

        public Production()

        {
        }

        //public void Working ( )
        //{
        //    resourceIn.Quantity = resourceIn.Quantity + (15 * this.Level);
        //    resourceOut.Quantity = resourceOut.Quantity - (15 * this.Level);
        //}

        public Production(String resourceIn, String resourceOut, String type, int buildingCost, int level, bool @lock) : base(type, buildingCost, level, @lock)
        {
            this.Type = type;
            this.BuildingCost = buildingCost;
            this.Level = level; 
            this.Lock = @lock;
            this.resourceIn = resourceIn;
            this.resourceOut = resourceOut;
        }

    }
}
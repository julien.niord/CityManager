﻿using CityManager.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CityManager.Models
{
    public class Storage : Building
    {
        public Storage()
        {
        }

        public Storage(string type, int buildingCost, int level, bool @lock) : base(type, buildingCost, level, @lock)
        {
        }

        public void ModifyMaxQuantity(Resource resource)
        {
            if (resource.GetType() == this.GetType())
            {
                resource.MaxQuantity = resource.MaxQuantity * (2 + this.Level);
            }
        }
    }
}
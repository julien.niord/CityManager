﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CityManager.Models.Base
{
    public abstract class ModelBase
    {
        private long? primaryKey;

        [Key]
        public long? PrimaryKey
        {
            get { return primaryKey; }
            set { primaryKey = value; }
        }
    }
}
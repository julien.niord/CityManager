﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CityManager.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class AuthDbContext : IdentityDbContext<ApplicationUser>
    {
        public AuthDbContext()
            : base("AuthConnection", throwIfV1Schema: false)
        {
            if (this.Database.CreateIfNotExists())
            {
                this.Roles.Add(new IdentityRole() { Name = "Admin" });
                this.Roles.Add(new IdentityRole() { Name = "Player" });
                this.SaveChanges();
                
                }
        }

        public static AuthDbContext Create()
        {
            return new AuthDbContext();
        }
    }

    public class GameDbContext : DbContext
    {
        public GameDbContext()
            : base("GameConnection")
        {
            this.Database.CreateIfNotExists();
            if (!this.Database.CompatibleWithModel(false))
            {
                this.Database.Delete();
                this.Database.CreateIfNotExists();
            }
        }

        public static GameDbContext Create()
        {
            return new GameDbContext();
        }

        public System.Data.Entity.DbSet<CityManager.Models.AppUser> AppUsers { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Building> Buildings { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Resource> Resources { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Storage> Storages { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Resident> Residents { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Rebel> Rebels { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.DMF> DMFs { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Civilian> Civilians { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Happiness> Happinesses { get; set; }

        public System.Data.Entity.DbSet<CityManager.Models.Production> Productions { get; set; }
    }
}
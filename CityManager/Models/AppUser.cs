﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CityManager.Models
{
    public class AppUser
    {
        [Key]
        public long PrimaryKey { get; set; }
        public String UserName { get; set; }
        public ICollection<Production> Buildings { get; set; }
        public ICollection<Resource> Resources { get; set; }
        public string ConnectedUser { get; internal set; }
    }
}
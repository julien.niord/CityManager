﻿using CityManager.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CityManager.Models
{
    public abstract class Building : ModelBase
    {
        protected Building()
        {
        }

        protected Building(string type, int buildingCost, int level, bool @lock)
        {
            Type = type;
            BuildingCost = buildingCost;
            Level = level;
            Lock = @lock;
        }

        public String Type { get; set; }
        public int BuildingCost { get; set; }
        [Range(0, 10)]
        public int Level { get; set; }
        public Boolean Lock { get; set; }
    }
}
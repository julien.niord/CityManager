﻿using CityManager.Controllers.Base;
using CityManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CityManager.Controllers
{
    public class ProductionsController : AdminControllerBase<Production>
    {


        protected GameDbContext dbContext = new GameDbContext();

        public override Task<ActionResult> Create()
        {
            ViewBag.Resources = dbContext.Set<Resource>().ToList();
            return base.Create();
        }


    }
}
﻿using CityManager.Controllers.Extensions;
using CityManager.Models.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CityManager.Models;

namespace CityManager.Controllers.Base
{
    [Authorize(Roles = "Admin, Player")]
    public class AdminControllerBase<T> : Controller where T : ModelBase, new()
    {
        protected GameDbContext dbContext = new GameDbContext();

        public virtual ActionResult Index()
        {
            List<T> items = dbContext.Set<T>().ToList();
            return View(items);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public virtual async Task<ActionResult> Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireRequestValue(new String[] { })]
        public virtual async Task<ActionResult> Create([Bind(Include = "")]T item)
        {
            if (ModelState.IsValid)
            {
                dbContext.Set<T>().Add(item);
                await dbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public virtual async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> DeleteConfirmed(int? id)
        {
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item != null)
            {
                dbContext.Set<T>().Remove(item);
                await dbContext.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public virtual async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }

            return View(item);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit([Bind(Include = "")] T item)
        {
            if (ModelState.IsValid)
            {
                dbContext.Entry(item).State = EntityState.Modified;
                await dbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(item);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public virtual async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            T item = await dbContext.Set<T>().FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                dbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
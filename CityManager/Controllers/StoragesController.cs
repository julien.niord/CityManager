﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CityManager.Controllers.Base;
using CityManager.Models;

namespace CityManager.Controllers
{
    public class StoragesController : AdminControllerBase<Storage>
    {

        protected GameDbContext dbContext = new GameDbContext();

        public override Task<ActionResult> Create()
        {
            ViewBag.Resources = dbContext.Set<Resource>().ToList();
            return base.Create();
        }

    }
}

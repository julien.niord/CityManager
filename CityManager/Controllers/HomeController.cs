﻿using CityManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Threading;
using static Microsoft.AspNet.Identity.Owin.OwinContextExtensions;

namespace CityManager.Controllers
{
	public class HomeController : Controller
	{
        protected GameDbContext dbContext = new GameDbContext();

        public async Task<ActionResult> Index()
		{
            ViewBag.Message = "Your application description page.";

            return View();
        }

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

        public async Task<ActionResult> User()
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            String userId = (await userManager.FindByNameAsync(Thread.CurrentPrincipal.Identity.Name)).Id;
            List<Resource> items = dbContext.Set<Resource>().ToList();
            AppUser user = await dbContext.AppUsers.Include(x => x.Buildings).Include(x => x.Resources).Where(x => x.ConnectedUser == userId).FirstOrDefaultAsync();
            ViewBag.User = user;
            return View(items);
        }
    }
}